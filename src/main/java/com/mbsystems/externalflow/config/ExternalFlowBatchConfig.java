package com.mbsystems.externalflow.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class ExternalFlowBatchConfig {

    private final JobBuilderFactory jobBuilderFactory;

    private final StepBuilderFactory stepBuilderFactory;

    public ExternalFlowBatchConfig( JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory ) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }

    @Bean
    public Tasklet localStockFileTasklet() {
        return ( ( stepContribution, chunkContext ) -> {
            System.out.println( "The stock file has been loaded" );
            return RepeatStatus.FINISHED;
        });
    }

    @Bean
    public Tasklet loadCustomerFileTasklet() {
        return ( ( stepContribution, chunkContext ) -> {
            System.out.println( "The customer file has been loaded" );
            return RepeatStatus.FINISHED;
        });
    }

    @Bean
    public Tasklet updateStartTasklet() {
        return ( ( stepContribution, chunkContext ) -> {
            System.out.println( "The start has been updated" );
            return RepeatStatus.FINISHED;
        });
    }

    @Bean
    public Tasklet runBatchTasklet() {
        return ( ( stepContribution, chunkContext ) -> {
            System.out.println( "The batch has been run" );
            return RepeatStatus.FINISHED;
        });
    }

    @Bean
    public Step loadFileStep() {
        return this.stepBuilderFactory.get( "loadFileStep" )
                        .tasklet( localStockFileTasklet() )
                        .build();
    }

    @Bean
    public Step loadCustomerStep() {
        return this.stepBuilderFactory.get( "loadCustomerStep" )
                        .tasklet( loadCustomerFileTasklet() )
                        .build();
    }

    @Bean
    public Step updateStartStep() {
        return this.stepBuilderFactory.get( "updateStartStep" )
                        .tasklet( updateStartTasklet() )
                        .build();
    }

    @Bean
    public Step runBatch() {
        return this.stepBuilderFactory.get( "runBatch" )
                        .tasklet( runBatchTasklet() )
                        .build();
    }

    @Bean
    public Flow preProcessingFlow() {
        return new FlowBuilder<Flow>( "preProcessingFlow" )
                        .start( loadFileStep() )
                        .next( loadCustomerStep() )
                        .next( updateStartStep() )
                        .build();
    }

    @Bean
    public Job conditionalStepLogicJob() {
        return this.jobBuilderFactory.get( "ExternalFlow" )
                        .start( preProcessingFlow() )
                        .next( runBatch() )
                        .end()
                        .build();
    }
}
